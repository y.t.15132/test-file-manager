<?php
namespace App\Models;

use App\Lib\FileInterface;
use JetBrains\PhpStorm\Pure;

Class File implements FileInterface {
    public function __construct(private string $name,private string $path='/public'){}

    public function setPath(string $path){
        $this->path = $path;
    }

    public function getPath():string{
        return $this->path;
    }

    #[Pure] public function getAddress():string{
        return $this->getPath() . $this->name;
    }

    public function getName():string{
        return $this->name;
    }
}
