<?php
namespace App\Lib\Helpers;

use App\Lib\Exceptions\RuntimeException;
use App\Lib\FileInterface;


class Stream
{
    /** @var resource $handle */
    protected $handle;

    protected ?int $lock = null;
    protected ?array $metadata = null;

    /**
     * @param resource $handle
     * @throws RuntimeException
     */
    public function __construct(mixed $handle)
    {
        if (!is_resource($handle)) {
            throw new RuntimeException(sprintf('file-handle must be of type \'resource\' but \'%s\' given', get_debug_type($handle)), 500);
        }

        $this->handle = $handle;
    }

    /**
     * @throws RuntimeException
     */
    public static function fromResourceName(FileInterface $file, string $mode = 'rb+'): self
    {
        try {
            $handle = fopen($file->getAddress(), $mode);
        } catch (\Exception $e){
            throw new RuntimeException("Stream creation failed, resource not found: ".$file->getAddress(), 500);
        }

        return new static($handle);
    }

    public function __destruct()
    {
        if ($this->lock !== null) {
            flock($this->handle, LOCK_UN);
        }

        if (is_resource($this->handle)) {
            fclose($this->handle);
        }
    }


    /**
     * @throws RuntimeException
     */
    public function lock(int $mode): self
    {
        if ($this->lock !== null) {
            return $this;
        }

        if ($mode === 0) {
            return $this;
        }

        if (!flock($this->handle, $mode | LOCK_NB)) {
            throw new RuntimeException('unable to get file-lock', 500);
        }

        $this->lock = $mode;
        return $this;
    }

    public function unlock(): self
    {
        if ($this->lock !== null) {
            flock($this->handle, LOCK_UN);
        }
        return $this;
    }

    /**
     * @throws RuntimeException
     */
    public function rewind(int $offset = 0): static
    {
        if (fseek($this->handle, $offset) !== 0) {
            throw new RuntimeException('error while rewinding file', 500);
        }

        return $this;
    }


    /**
     * @throws RuntimeException
     */
    public function write(string $content): void
    {
        if (false === fwrite($this->handle, $content)) {
            throw new RuntimeException('Error while writing to stream.', 500);
        }
    }

    /**
     * @throws RuntimeException
     */
    public function read(int $offset = 0, ?int $length = null): string
    {
        $this->rewind($offset);

        if ($length === null) {
            $filesize = fstat($this->handle)['size'] ?? 0;
            $length = $filesize - $offset;
        }

        if ($length <= 0) {
            return '';
        }

        // read part of file
        if (false === $result = fread($this->handle, $length)) {
            throw new RuntimeException('Error while reading stream.', 500);
        }

        return $result;
    }


    /**
     * @return resource
     */
    public function getHandle()
    {
        return $this->handle;
    }

    public function getMetaData(): array
    {
        if ($this->metadata === null) {
            $this->metadata = stream_get_meta_data($this->handle);
        }
        return $this->metadata;
    }

    /**
     * @internal
     */
    public function updateMetaData(array $metadata): void
    {
        $this->metadata = array_replace($this->getMetaData(), $metadata);
    }
}
