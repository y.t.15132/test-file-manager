<?php
namespace App\Services;
use App\Lib\Exceptions\RuntimeException;
use App\Lib\FileInterface;
use App\Lib\Helpers\Stream;
use App\Lib\StorageInterface;

class Disk implements StorageInterface {

    public function __construct(private FileInterface $path){}

    /**
     * @return bool
     * @throws \Exception
     */
    public function putFile(string $content): bool
    {
        $mode = 'w';
        $stream = $this->getStream($mode);
        try {
            $stream->write($content);
            return true;
        } finally {
            $stream->unlock();
        }
    }

    /**
     * @throws \Exception
     */
    private function getStream(string $mode = 'rb+'): Stream
    {
        return Stream::fromResourceName($this->path, $mode);
    }

    /**
     * @return string
     * @throws \App\Lib\Exceptions\RuntimeException
     * @throws \Exception
     */
    public function getFileContent(): string
    {
        $mode = 'r';
        $stream = $this->getStream($mode);
        try {
            return $stream->read();
        } catch (RuntimeException $exception){
            throw new RuntimeException('Error Reading file '.$exception->getMessage());
        }
    }

    /**
     * @return array
     * @throws RuntimeException
     */
    public function getFileMetadata(): array
    {
        $mode = 'r';
        $stream = $this->getStream($mode);

        try {
            return $stream->getMetaData();
        } catch (RuntimeException $exception){
            throw new RuntimeException('Error Reading file '.$exception->getMessage());
        }
    }

    /**
     * @return bool
     */
    public function removeFile(): bool
    {
        if (!unlink($this->path->getAddress())) {
            return false;
        }
        return true;
    }

}
