<?php
namespace App\Lib;

interface StorageInterface {
    public function putFile(string $content):bool;
    public function getFileContent():string;
    public function getFileMetadata():array;
    public function removeFile():bool;

}
