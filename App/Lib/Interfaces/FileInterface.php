<?php
namespace App\Lib;

interface FileInterface {
    public function setPath(string $path);
    public function getPath():string;
    public function getAddress():string;
    public function getName():string;
}
