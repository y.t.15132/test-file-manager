<?php
namespace App\Controllers;

class FileRepository {

    public static function downloadFile($fileName,$content)
    {
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($fileName) . "\"");
        echo $content;
    }

    public static function printMetadata($metadata){
        dd($metadata);
    }


}
