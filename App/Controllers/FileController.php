<?php
namespace App\Controllers;
use App\Models\File;
use App\Services\Disk;

class FileController {

    private ?Disk $disk = null;
    public function __construct($fileName)
    {
        $file = new File($fileName);
        $file->setPath(__DIR__.'/../../public/');
        $this->disk = new Disk($file);
    }

    /**
     * @throws \App\Lib\Exceptions\RuntimeException
     */
    public function getFileContent():string
    {
        return $this->disk->getFileContent();
    }

    /**
     * @throws \App\Lib\Exceptions\RuntimeException
     */
    public function getMetadata(): array
    {
        return $this->disk->getFileMetadata();
    }

    public function removeFile():bool
    {
        return $this->disk->removeFile();
    }


    /**
     * @throws \Exception
     */
    public function putFile(string $fileName , string $content):bool
    {
        $file = new File($fileName);
        $file->setPath(__DIR__.'/../../public/');

        $disk = new Disk($file);
        return $disk->putFile($content);
    }

}
