<?php

use App\Models\File;
use App\Services\Disk;
use PHPUnit\Framework\TestCase;

class FileActionsTest extends TestCase
{

    public $fileName = "test_file.jpeg";
    public $tempFileName = 'temp_file.jpeg';
    public $path = __DIR__.'/../public/';
    public $disk = null;

    protected function setUp(): void
    {
        $this->createTempFile();
    }

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $file = new File($this->fileName);
        $file->setPath($this->path);
        $this->disk = new Disk($file);
    }

    public function test_read_file()
    {
        $content = $this->disk->getFileContent();
        $this->assertIsString($content);
    }

    public function test_get_metadata()
    {
        $content = $this->disk->getFileMetadata();
        $this->assertIsArray($content);
    }

    public function test_remove_file()
    {
        $file = new File($this->tempFileName);
        $file->setPath($this->path);
        $newDisk = new Disk($file);
        $newDisk->removeFile();

        $isFileStillThere = @file_get_contents($this->path.$this->tempFileName);
        $this->assertTrue(!$isFileStillThere);
    }

    private function createTempFile(){
        copy($this->path.$this->fileName,$this->path.$this->tempFileName);
    }

}
