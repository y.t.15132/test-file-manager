<?php
include 'vendor/autoload.php';

$fileController = new App\Controllers\FileController("test_file.jpeg");


//Routes
switch($_GET['route']) {
    case 'read':
        \App\Controllers\FileRepository::downloadFile('Download_file.jpeg',$fileController->getFileContent());
        break;
    case 'getMetadata':
        try {
            \App\Controllers\FileRepository::printMetadata($fileController->getMetadata());
        } catch (\App\Lib\Exceptions\RuntimeException $e) {
            echo $e->getMessage();
        }
        break;

    case 'remove':
        echo $fileController->removeFile() ? 'Removed' : 'Not Removed';
        break;

    case 'put':
        try {
            $fileController->putFile('/copy-test-file.jpeg',$fileController->getFileContent());
            echo "File Inserted Successfully";
        } catch (Exception $e) {
            echo $e->getMessage();
        }
}



